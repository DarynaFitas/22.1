using System.Linq;


namespace vector
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            
                int n = int.Parse(textBox1.Text);
                double[] x = new double[n];
                double[] y = new double[n];
               

                string[] lines;
                lines = textBox2.Text.Split(' ');
                for (int i = 0; i < n; i++)
                {
                    x[i] = double.Parse(lines[i]);
                }

                lines = textBox3.Text.Split(' ');
                for (int i = 0; i < n; i++)
                {
                    y[i] = double.Parse(lines[i]);
                }
            double[] z = x.Where(c => c > 0).Concat(y.Where(c => c > 0)).ToArray();

        
                textBox4.Text = "The vector z is:\r\n";
                for (int i = 0; i < 2 * n; i++)
                {
                    textBox4.Text += z[i].ToString() + " ";
                }
            }

    
    }
}